import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView  from "../views/LoginView.vue";
import CadastroView  from "../views/CadastroView.vue";
import CadastroCervejaView  from "../views/CadastroCervejaView.vue";
import SelecionaCervejaView  from "../views/SelecionaCervejaView.vue";
import RanqueamentoView  from "../views/RanqueamentoView.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ranqueamento',
    component: RanqueamentoView
  },
  {
    path: '/cadastrocerveja',
    name: 'cadastrocerveja',
    component: CadastroCervejaView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/selecionacerveja',
    name: 'selecionaCerveja',
    component: SelecionaCervejaView
  },
  {
    path: '/cadastro',
    name: 'cadastro',
    component: CadastroView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
