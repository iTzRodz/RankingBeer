<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\RankingController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\BeerController;
use Carbon\Carbon;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('/cadastrar', [UsuarioController::class, 'store']);
$router->group(['prefix' => 'usuario'], function ($router) {
    $router->post('/cadastro', [UsuarioController::class, 'store']);
});


Route::apiResource('/ranking',RankingController::class);


Route::apiResource('/evaluation',EvaluationController::class);

Route::apiResource('/beer', BeerController::class);