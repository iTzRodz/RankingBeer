# Back-End

# Como Instalar?

Rodar os seguintes comandos em sequência

1. `composer install`
2. `php artisan key:generate`
3. `php artisan migrate`
4. `php artisan db:seed`
5. `php artisan serve`

#### OBS
**Ponto importante para rodar o segundo comando**
Quando clonar o repositorio, não vai vir o arquivo **.env**, apenas o **.envexample** basta renomear ele para .env. Agora podemos rodar 
![Exemplo de configuração bd](../public/ConfigBd.png "Exemplo de configuração bd"). 
Agora sim podemos rodar todos os comandos.