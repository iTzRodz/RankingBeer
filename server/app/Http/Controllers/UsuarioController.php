<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Illuminate\Support\Facades\Hash;


class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Usuario::all();

        return response()->json($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Usuario $usuario)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'nick_name' => 'max:20',
            'birth_date' => 'required|date',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|max:255',
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Sua requisição está faltando dados'
            ], 400);
        };

        $birth_date = new DateTime($request['birth_date']);
        $usuario->birth_date = $birth_date->format("Y-m-d");
        Usuario::create([
            'name' => $request->name,
            'birth_date' => $request->birth_date,
            'nick_name' => $request->nick_name,
            'email' => $request->email,
            'password' => Hash::make($request['password'])
        ]);

        return response()->json([
            'message' => 'Usuario Cadastrado com sucesso'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
