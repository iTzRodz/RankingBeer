<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evaluation;
use Illuminate\Support\Facades\DB;


class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $rank = DB::select("select a.brand marca, a.name, avg(b.note) notas, avg(b.value) valor
        from beers as a
        join evaluations as b on (a.id = b.beer_id)
        group by a.name, a.brand");

        return response()->json($rank);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beer = DB::table('evaluations')
            ->join('beers', 'evaluations.beer_id', '=', 'beers.id')
            ->select(DB::raw('beers.name, beers.brand'))
            ->where('beer_id', $id)->distinct()->get();

        $gradeAverage = DB::table('evaluations')
            ->join('beers', 'evaluations.beer_id', '=', 'beers.id')
            ->where('beer_id', $id)
            ->avg('evaluations.note');

        $valueAverage = DB::table('evaluations')
            ->join('beers', 'evaluations.beer_id', '=', 'beers.id')
            ->where('beer_id', $id)
            ->avg('evaluations.value');

        $result = [
            "Cerveja" => $beer,
            "Média nota" => $gradeAverage,
            "Média valor" => 'R$' . $valueAverage
        ];

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
