<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated =  Validator::make($request->all(), [
            'note' => 'required|numeric',
            'value' => 'required|numeric',
            'beer_id' => 'required',
            'usuario_id' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Sua requisição está faltando dados'
            ], 400);
        };

        Evaluation::create($request->all());

        
        return response()->json([
            'message' => 'Avaliação feita com sucesso'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $myRatings = DB::table('evaluations')
            ->join('beers', 'evaluations.beer_id', '=', 'beers.id')
            ->select('beers.name', 'beers.brand', 'evaluations.note', 'evaluations.value')
            ->where('usuario_id', $id)
            ->get();

        return response()->json($myRatings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evaluation $evaluation)
    {
        $validated =  Validator::make($request->all(), [
            'note' => 'required|numeric',
            'value' => 'required|numeric',
            'beer_id' => 'required',
            'usuario_id' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Sua requisição está faltando dados'
            ], 400);
        };

        $evaluation->update($request->all());

        return response()->json([
            'message' => 'Atualização feita com sucesso'
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evaluation $evaluation)
    {
        $evaluation->delete();

        return response()->json([
            'message' => 'Avaliação deletada com sucesso',
        ]);
    }
}
