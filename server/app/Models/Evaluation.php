<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use HasFactory;

    protected $fillable = [
        'note',
        'value',
        'beer_id',
        'usuario_id'

    ];

    public function beers()
    {
        return $this->belongsTo(Beer::class);
    }

    public function usuarios()
    {
        return $this->hasOne(Usuario::class);
    }
}
