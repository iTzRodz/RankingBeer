<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beer extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'brand',

    ];

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class, 'beer_usuario');
    }

    public function evaluations()
    {
        return $this->hasOne(Evaluations::class);
    }
}
