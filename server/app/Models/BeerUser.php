<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeerUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'usuario_id',
        'beer_id'
    ];

    // public function beers()
    // {
    //     return $this->belongsToMany(Beer::class);
    // }

    // public function users()
    // {
    //     return $this->belongsToMany(User::class);
    // }
}
