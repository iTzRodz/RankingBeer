<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',  'nick_name', 'birth_date', 'email', 'password'
    ];

    public function beers()
    {
        return $this->belongsToMany(Beer::class, 'beer_usuario');
    }

    public function evaluations()
    {
        return $this->belongsTo(Evaluation::class);
    }
}


