<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beer_usuario', function (Blueprint $table) {
            $table->foreignId('usuario_id')->constrained();
            $table->foreignId('beer_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beer_usuario', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['beer_id']);
        });

        Schema::dropIfExists('beer_usuario');
    }
};
