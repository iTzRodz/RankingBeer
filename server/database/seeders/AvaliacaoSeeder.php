<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AvaliacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('evaluations')->insert([
            [
                'note' => 4.5,
                'value' => 14.50,
                'usuario_id' => 1,
                'beer_id' => 3
            ],
            [
                'note' => 4.5,
                'value' => 14.50,
                'usuario_id' => 8,
                'beer_id' => 3
            ],
            [
                'note' => 3,
                'value' => 4.50,
                'usuario_id' => 3,
                'beer_id' => 8
            ],
            [
                'note' => 4.5,
                'value' => 4.50,
                'usuario_id' => 1,
                'beer_id' => 6
            ],
            [
                'note' => 2,
                'value' => 1.50,
                'usuario_id' => 2,
                'beer_id' => 3
            ],
            [
                'note' => 4,
                'value' => 12.50,
                'usuario_id' => 3,
                'beer_id' => 2
            ],
            [
                'note' => 2,
                'value' => 1.89,
                'usuario_id' => 1,
                'beer_id' => 4
            ],
            [
                'note' => 1,
                'value' => 1.50,
                'usuario_id' => 3,
                'beer_id' => 10
            ],
            [
                'note' => 0,
                'value' => 0.50,
                'usuario_id' => 2,
                'beer_id' => 12
            ],
            [
                'note' => 5,
                'value' => 12.50,
                'usuario_id' => 1,
                'beer_id' => 2
            ],
            [
                'note' => 4.5,
                'value' => 10.50,
                'usuario_id' => 1,
                'beer_id' => 2
            ],
            [
                'note' => 5,
                'value' => 1.50,
                'usuario_id' => 8,
                'beer_id' => 2
            ],
            [
                'note' => 4,
                'value' => 50,
                'usuario_id' => 7,
                'beer_id' => 10
            ],
            [
                'note' => 5,
                'value' => 150,
                'usuario_id' => 1,
                'beer_id' => 10
            ],
            [
                'note' => 2,
                'value' => 100,
                'usuario_id' => 6,
                'beer_id' => 10
            ],
            [
                'note' => 3.5,
                'value' => 1.40,
                'usuario_id' => 6,
                'beer_id' => 5
            ],
            [
                'note' => 2.5,
                'value' => 4.50,
                'usuario_id' => 7,
                'beer_id' => 8
            ],
            [
                'note' => 4.5,
                'value' => 3.50,
                'usuario_id' => 1,
                'beer_id' => 8
            ],
            [
                'note' => 1.5,
                'value' => 5,
                'usuario_id' => 7,
                'beer_id' => 8
            ],
            [
                'note' => 3.5,
                'value' => 1.50,
                'usuario_id' => 5,
                'beer_id' => 7
            ],
            [
                'note' => 4.5,
                'value' => 10.50,
                'usuario_id' => 1,
                'beer_id' => 10
            ],
            [
                'note' => 4.5,
                'value' => 1.50,
                'usuario_id' => 4,
                'beer_id' => 5
            ],
            [
                'note' => 4.5,
                'value' => 10.50,
                'usuario_id' => 1,
                'beer_id' => 10
            ],
            

        ]);
    }
}
