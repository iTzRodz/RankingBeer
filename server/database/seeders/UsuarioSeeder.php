<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            [   'name' => 'usuario teste',
                'nick_name' => 'teste',
                'birth_date' => '2000/10/10',
                'email' => 'teste@example2.com',
                'password' => hash::make('umhashai')
            ],
            [   'name' => 'usuario teste 2',
                'nick_name' => 'teste2',
                'birth_date' => '2000/10/10',
                'email' => 'teste2@example2.com',
                'password' => hash::make('lalalal')
            ],
            [   
                'name' => 'usuario teste 3',
                'nick_name' => 'teste3',
                'birth_date' => '2000/10/10',
                'email' => 'teste3@example.com',
                'password' => hash::make('valdircareca')
            ],
            [   'name' => 'kratos',
                'nick_name' => 'God o War',
                'birth_date' => '10/10/10',
                'email' => 'grego@viking.com',
                'password' => hash::make('comitodas')
            ],
            [   'name' => 'atreus',
                'nick_name' => 'fi do God o War',
                'birth_date' => '210/10/10',
                'email' => 'loki@gay.com',
                'password' => hash::make('elemecomeu')
            ],
            [
                'name' => 'Leonardo',
                'nick_name' => 'leo berranteiro',
                'birth_date' => '2003/10/9',
                'email' => 'leoleoleo@deleo.com',
                'password' => hash::make('senhasegura')
            ],
            [
                'name' => 'atreus',
                'nick_name' => 'fi do God o War',
                'birth_date' => '10/10/10',
                'email' => 'loki@heteru.com',
                'password' => hash::make('elemecomeu')
            ],
            [
                'name' => 'rodolfo',
                'nick_name' => 'rodz',
                'birth_date' => '2003/04/30',
                'email' => 'rodolfo@hotmen.com',
                'password' => hash::make('coxinha123')
            ],
            
                            
        ]);
    }
}
