<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Beer;


class CervejaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('beers')->insert([
            [
                'name' => 'Brahma Larger',
                'brand' => 'Brahma'
            ],
            [
                'name' => 'Sub-zero',
                'brand' => 'Antarctica'
            ],
            [
                'name' => 'Skol',
                'brand' => 'Skol Larger'
            ],
            [
                'name' => 'Conti-Zero',
                'brand' => 'Conti'
            ],
            [
                'name' => 'Bohemian Pilsen',
                'brand' => 'Patagonia'
            ],
            [
                'name' => 'Budweiser Lager',
                'brand' => 'Budweiser'
            ],
            [
                'name' => 'Caracu Sweet stout',
                'brand' => 'Caracu'
            ],
            [
                'name' => 'Colorado Appia Sleek',
                'brand' => 'Colorado'
            ],
            [
                'name' => 'Corona Pilsen',
                'brand' => 'Corona'
            ],
            [
                'name' => 'Conti Pilsen',
                'brand' => 'Conti'
            ],
            [
                'name' => 'Brahma Dublo malter',
                'brand' => 'Brahma'
            ],
            [
                'name' => 'Skol puro Malte',
                'brand' => 'Skol'
            ],
            [
                'name' => 'Serrane Larger',
                'brand' => 'Serrana'
            ],
            [
                'name' => 'Artartica Original',
                'brand' => 'Artartica Original Larger',
            ],


        ]);
    }
}
